import numpy as np

from .flight_bookings import get_probability_mass_function, \
    get_expected_value, \
    get_min_number_sold, simulate_sale, run_simulation, get_expected_from_bayes


def test_pmf_simple():
    pmf = get_probability_mass_function(p_show=.5, n_sold=1)
    p_1 = pmf[1]
    assert p_1 == .5
    assert pmf[0] == 0.5
    assert pmf[1.2] == 0


def test_pmf_still_simple():
    pmf = get_probability_mass_function(p_show=1, n_sold=2)
    assert pmf[0] == 0.
    assert pmf[1] == 0.
    assert pmf[2] == 1.


def test_pmf_less_simple():
    pmf = get_probability_mass_function(p_show=0.5, n_sold=2)
    assert pmf[0] == .25
    assert pmf[1] == 0.50
    assert pmf[2] == 0.25


def test_pmf_three_sold():
    pmf = get_probability_mass_function(p_show=0.5, n_sold=10)
    assert pmf[0] == pmf[10] == 0.5**10


def test_exp_simple():
    e = get_expected_value(p_show=1, n_sold=1)
    assert e == 1

    e = get_expected_value(p_show=1, n_sold=100)
    assert e == 100


def test_exp_less_simple():
    e = get_expected_value(p_show=0.5, n_sold=50)
    assert e == 25


def test_homework_422a():
    e = get_expected_value(p_show=0.9, n_sold=6)
    assert e == 5.4


def test_homework_422b():
    e = get_expected_value(p_show=0.7, n_sold=9)
    assert e > 6
    assert e < 7


def test_homework_422b_again():
    req = get_min_number_sold(p_show=0.7, n_required=6)
    assert req == 9


def test_homework_423a():
    req = get_min_number_sold(p_show=0.7, n_required=11)
    assert req == 16


def test_simulation():
    tickets = simulate_sale(n_sold=10, p_show=1.)
    assert tickets.shape == (10,)
    assert np.sum(tickets) == 10


def test_homework_423b():
    e = run_simulation(its=100, n_sold=10, p_show=0.7, threshold=2)
    assert e > 0


def test_bayes():
    e_bayes = get_expected_from_bayes(p_show=0.7, n_sold=10)
    e_simulated = run_simulation(p_show=0.7, n_sold=10, threshold=2, its=1000)
    assert np.isclose(e_bayes, e_simulated, atol=.1)
