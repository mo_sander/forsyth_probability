import math
from collections import defaultdict
import numpy as np

rg = np.random.default_rng()


def simulate_sale(n_sold: int, p_show: float):
    return rg.binomial(p=p_show, n=1, size=n_sold)


def get_events_below_threshold(x: int, threshold: int):
    n = 0
    for i in range(threshold):
        n += math.comb(x, i)
    return n


def get_p_flight_given_x(x: int, threshold: int = 2):
    p_flight_given_x = (2**x - get_events_below_threshold(x, threshold)) / 2**x
    return p_flight_given_x


def get_p_liftoff(n_sold: int, p_show: float, threshold=2):
    p_y = sum([get_probability_mass_function(p_show=0.5 * p_show,
                                             n_sold=n_sold)[i] for i in
               range(threshold, n_sold + 1)])
    return p_y


def get_px_given_flight(x: int, p_show: float, n_sold: int=10):
    p_x = get_probability_mass_function(p_show=p_show, n_sold=n_sold)[x]
    p_flight_given_x = get_p_flight_given_x(x)
    p_y = get_p_liftoff(n_sold=n_sold, p_show=p_show)

    p_bayes = (p_x * p_flight_given_x) / p_y
    return p_bayes


def get_expected_from_bayes(p_show: float, n_sold: int):
    return sum([(x * get_px_given_flight(x=x, p_show=p_show, n_sold=n_sold))
                    for x in range(n_sold + 1)])


def check_liftoff(tickets: np.array, threshold: int):
    n_females = np.sum(rg.choice([0, 1], size=tickets[tickets == 1].size))
    if n_females >= threshold:
        return True
    return False


def run_simulation(its: int, n_sold: int, p_show: float, threshold: int):
    values = []
    for x in range(its):
        sample = simulate_sale(n_sold, p_show)
        if check_liftoff(sample, threshold):
            values.append(np.sum(sample))
    return np.mean(values)


def get_probability_mass_function(p_show: float, n_sold: int):
    pmf = defaultdict(lambda: 0.)
    p_no_show = 1 - p_show

    for i in range(n_sold + 1):
        n_events = math.comb(n_sold, i)
        pmf[i] = (n_events * (p_show ** i)) * (p_no_show ** (n_sold - i))
    return pmf


def get_expected_value(p_show: float, n_sold: int):
    pmf = get_probability_mass_function(p_show=p_show, n_sold=n_sold)
    return sum([pmf[x] * x for x in range(n_sold + 1)])


def get_min_number_sold(p_show: float, n_required: int):
    i = 0
    while True:
        e = get_expected_value(p_show, i)
        if e >= n_required:
            return i
        if i > 1000:
            raise RuntimeError("selling a lot of tickets now :(")
        i += 1


def main():
    e = run_simulation(its=1000, n_sold=10, p_show=0.7, threshold=2)
    print(e)


if __name__ == "__main__":
    main()
