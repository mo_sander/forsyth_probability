from jax import numpy
from probability.weak_law_of_large_n import draw_from_binomial


def test_draw_from_binomial():
    e = draw_from_binomial(repeats=10, N=20)
    assert numpy.mean(e, axis=1).shape == (10,)
