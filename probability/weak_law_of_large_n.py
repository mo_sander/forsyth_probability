import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

rg = np.random.default_rng()


def draw_from_binomial(repeats: int, N: int) -> np.ndarray:
    return rg.choice([-1, 1], size=(repeats, N))


def convert_to_expected_value(events: np.ndarray) -> np.ndarray:
    return np.mean(events, axis=1)


def convert_to_variance(events: np.ndarray) -> np.ndarray:
    return np.ma.var(events, axis=0)


def create_boxplot(xns):
    ax = sns.boxplot(x='x', y='y', data=xns)
    plt.show()


def show_boxplot_for_binomial():
    es = []
    vs = []
    xs = []
    vxs = []
    for x in [10, 20, 40, 80, 100]:
        events = draw_from_binomial(repeats=1000, N=x)
        xns = convert_to_expected_value(events)
        vns = convert_to_variance(xns)
        vs.append(vns)
        vxs.append(1/x)
        es.append(xns)
        xs.append([x] * 1000)

    v_df = pd.DataFrame(np.stack([vxs, vs], axis=-1), columns=["x", "v"])
    ax = sns.scatterplot(data=v_df, x="x", y="v")
    plt.show()

    ys = np.concatenate(es)
    xs = np.concatenate(xs)
    df = pd.DataFrame(np.stack([xs, ys], axis=-1), columns=["x", "y"])

    create_boxplot(df)


def main():
    show_boxplot_for_binomial()


if __name__ == "__main__":
    main()
