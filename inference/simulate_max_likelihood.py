from enum import Enum, auto
from typing import Tuple, List

import pandas
from sklearn.linear_model import LogisticRegression

from jax import random, numpy
from time import gmtime


class Distributions(Enum):
    NORMAL = auto()
    POISSON = auto()


class SimulateMaxLikelihood:
    def __init__(self, seed: int = gmtime().tm_sec):
        self.key = random.PRNGKey(seed)

    def draw_samples_from_normal(self, shape: Tuple):
        self.key, subkey = random.split(key=self.key)
        samples = random.normal(key=subkey, shape=shape)
        return samples

    def draw_samples_from_bernoulli(self, shape: Tuple, p: float):
        self.key, subkey = random.split(key=self.key)
        samples = random.bernoulli(key=subkey, p=p, shape=shape)
        return samples

    def draw_samples_from_poisson(self, shape: Tuple, intensity: float):
        self.key, subkey = random.split(key=self.key)
        samples = random.poisson(key=subkey, lam=intensity, shape=shape)
        return samples

    def draw_from_normal_conditional_on_sample(self, mask: numpy.ndarray,
                                               mean1: float, mean2: float):
        sample = self.draw_samples_from_normal(shape=mask.shape)
        shifted_1 = sample + mean1
        adjusted = mask * (mean2 - mean1)
        return shifted_1 + adjusted

    @staticmethod
    def determine_bernoulli_p_given_x_and_theta(x: numpy.ndarray,
                                                theta: numpy.ndarray):
        e = numpy.exp(numpy.dot(x, theta))
        return e/(1 + e)

    def generate_log_r_sample(self, theta: numpy.ndarray, n: int):
        xi = self.draw_samples_from_normal(shape=(n, theta.shape[0]))
        yi = self.draw_samples_from_bernoulli(
            shape=(n,),
            p=self.determine_bernoulli_p_given_x_and_theta(x=xi, theta=theta)
        )
        return xi, yi

    def train_logistic_regression(self, x: numpy.ndarray, y: numpy.ndarray):
        lr = LogisticRegression()
        lr.fit(x, y)
        return lr

    @staticmethod
    def estimate_normal_max_likelihood(samples):
        return numpy.mean(samples, axis=1)

    @staticmethod
    def estimate_poisson_max_likelihood(event_counts: numpy.ndarray,
                                        alpha: float, beta: float):
        """
        Estimate intensity parameter for Poisson distribution using the
        gamma distribution as a conjugate prior.

        :param alpha: gamma distribution parameter.
        :param beta: beta distribution parameter.
        :return:
        :param event_counts: histogram of counts, shape (n_samples, n_intervals)
        """
        N = numpy.sum(event_counts, axis=1)
        n_samples, n_bins = event_counts.shape
        range_vec = numpy.arange(n_bins, dtype=numpy.int32)
        if n_samples == 1:
            range_vec = numpy.expand_dims(range_vec, axis=0)
        total_events = numpy.sum(event_counts * range_vec, axis=1)

        estimated_intensity = (alpha - 1 + total_events) / (beta + N)
        return estimated_intensity

    @staticmethod
    def estimate_max_likelihood_std_var(type: Distributions, mean: float,
        samples: numpy.ndarray):
        if type == Distributions.NORMAL:
            return numpy.sqrt(numpy.mean((samples - mean)**2, axis=1))

    def simulate_normal_sample(self, shape: Tuple, mean: float, stdev: float):
        simul_sample = self.draw_samples_from_normal(shape=shape)
        return (simul_sample * stdev) + mean


def run_9_15_iteration(s, k):
    simulator = SimulateMaxLikelihood()
    samples = simulator.draw_samples_from_normal((s, k))
    mls = simulator.estimate_normal_max_likelihood(samples)
    print(f'The MLE for {s} samples of size {k} is {numpy.mean(mls):.3f}, '
          f'assuming a normal distribution of the data.')
    print(f'The variance of the estimates is {numpy.var(mls):.4f}.')


def run_9_15():
    for x in (5, 50, 500):
        run_9_15_iteration(100, x)


def run_9_16(p: float, mean1: float, mean2: float):
    simulator = SimulateMaxLikelihood()
    bernoulli_sample = simulator.draw_samples_from_bernoulli(shape=(100,), p=p)
    conditional_normal_sample = \
        simulator.draw_from_normal_conditional_on_sample(mask=bernoulli_sample,
                                                         mean1=mean1,
                                                         mean2=mean2)
    mle = conditional_normal_sample >= numpy.mean(numpy.array([mean1, mean2]))
    agreement = numpy.equal(bernoulli_sample, mle)
    print(f'The estimate is correct for {numpy.sum(agreement)} samples.')


def run_9_17_c():
    simulator = SimulateMaxLikelihood()
    theta = simulator.draw_samples_from_normal(shape=(10, ))
    thetas = []
    for x in range(100):
        xi, yi = simulator.generate_log_r_sample(theta=theta, n=1000)
        thetas.append(
            simulator.train_logistic_regression(xi, yi).coef_
        )

    mean_inferred = numpy.mean(numpy.array(thetas).squeeze(), axis=0)
    print(f'Real theta: {theta}')
    print(f'Inferred theta: {mean_inferred}')


def run_9_17_d():
    simulator = SimulateMaxLikelihood()
    theta = simulator.draw_samples_from_normal(shape=(10, ))
    xi1, yi1 = simulator.generate_log_r_sample(theta=theta, n=10)
    lr = simulator.train_logistic_regression(xi1, yi1)

    xi2, yi2 = simulator.generate_log_r_sample(theta=theta, n=1000)
    preds = lr.predict(xi2)
    equal = numpy.equal(yi2, preds)
    print(f'Correct predictions ratio: {numpy.sum(equal * 1) / len(equal):.3f}')


def run_9_18_a(location: str, s: int):
    df = pandas.read_csv(location).filter(items=["Diet", "Weight2"])
    sample = numpy.expand_dims(
        numpy.array(df[df["Diet"] == "chow"].sample(
            n=s).Weight2.dropna()),
        axis=0
    )

    simulator = SimulateMaxLikelihood()
    mean_param = numpy.squeeze(
        simulator.estimate_normal_max_likelihood(sample)
    )
    std_dev_param = simulator.estimate_max_likelihood_std_var(
        Distributions.NORMAL, mean=mean_param, samples=sample
    )

    simulated_sample = simulator.simulate_normal_sample(shape=(1000, s),
                                                        mean=mean_param,
                                                        stdev=std_dev_param)
    mean_weight = numpy.mean(simulated_sample, axis=1)
    conf_int = numpy.quantile(mean_weight,
                              numpy.array([0.125, 0.875], dtype=numpy.float32),
                              axis=0)
    print(f'Simulation-inferred mean weight is {mean_param:.3f}'
          f'\tci: {conf_int[0]:.3f} - {conf_int[1]:.3f}.')


def run_9_18_b(location: str, s: int):

    simulator = SimulateMaxLikelihood()

    df = pandas.read_csv(location).filter(items=["Diet", "Weight2"])

    sampled_means = []

    for x in range(1000):
        sample = numpy.expand_dims(
            numpy.array(df[df["Diet"] == "chow"].sample(
                n=s).Weight2.dropna()),
            axis=0
        )
        mean = simulator.estimate_normal_max_likelihood(sample)
        sampled_means.append(numpy.squeeze(mean))

    sampled_means = numpy.array(sampled_means, dtype=numpy.float32)
    mean_weight = numpy.mean(sampled_means, axis=0)
    conf_int = numpy.quantile(sampled_means,
                              numpy.array([0.125, 0.875], dtype=numpy.float32),
                              axis=0)

    print(f'Sampling-inferred mean weight is {mean_weight:.3f}'
          f'\t\tci: {conf_int[0]:.3f} - {conf_int[1]:.3f}.')

    all_samples = numpy.array(df[df["Diet"] == "chow"].Weight2.dropna())
    print(f'Real sample mean is '
          f'{numpy.mean(all_samples, axis=0):.3f}')


def run_9_19():
    first_10_intervals = numpy.array([5, 2, 2, 1, 0])
    next_20_intervals = numpy.array([9, 5, 3, 2, 1])
    histogram_all_intervals = first_10_intervals + next_20_intervals

    total_intervals = numpy.sum(histogram_all_intervals)
    bins = [x for x in range(len(histogram_all_intervals) + 10)]

    simulator = SimulateMaxLikelihood()
    for a in (0, 1.0, 5.0, 15.):
        for b in (0, 1.0,  5.0, 15.):
            intensity_lambda = simulator.estimate_poisson_max_likelihood(
                event_counts=numpy.expand_dims(histogram_all_intervals, axis=0),
                alpha=a,
                beta=b
            )
            samples = simulator.draw_samples_from_poisson(
                shape=(1000, total_intervals),
                intensity=intensity_lambda[0]
            )
            histograms = interval_counts_to_hist(samples, bins=bins)
            intensity_mle = simulator.estimate_poisson_max_likelihood(
                event_counts=histograms,
                alpha=a,
                beta=b
            )
            conf_int = numpy.quantile(
                intensity_mle,
                numpy.array([.05, .95], dtype=numpy.float32),
                axis=0
            )
            print(f'alpha: {a}, beta: {b} \t MLE:'
                  f' {intensity_lambda[0]:.3f}'
                  f'\tci size: {conf_int[1] - conf_int[0]:.3f}.')


def interval_counts_to_hist(samples, bins: List):
    return numpy.apply_along_axis(
        lambda a: numpy.histogram(a, bins=bins)[0], 1, samples)


if __name__ == "__main__":
    run_9_15()
    run_9_16(.8, 1.2, 1)
    run_9_17_c()
    run_9_17_d()
    run_9_18_a(location="~/Downloads/Churchill.Mamm.Gen.2012.phenotypes.csv",
               s=30)
    run_9_18_b(location="~/Downloads/Churchill.Mamm.Gen.2012.phenotypes.csv",
               s=30)
    run_9_19()
