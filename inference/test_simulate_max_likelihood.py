import jax
import pytest
from jax import numpy
from .simulate_max_likelihood import Distributions, SimulateMaxLikelihood


def test_draw_samples():
    sut = SimulateMaxLikelihood()
    sample = sut.draw_samples_from_normal(shape=(3, 67))
    assert sample.shape == (3, 67)


def test_max_likelihood():
    sut = SimulateMaxLikelihood()
    sample = numpy.array([[2, 2, 2], [1, 1, 1]])
    mls = sut.estimate_normal_max_likelihood(samples=sample)
    assert mls.shape == (2,)
    assert mls[0] == 2
    assert mls[1] == 1


def test_bernoulli():
    sut = SimulateMaxLikelihood(seed=42)
    sample = sut.draw_samples_from_bernoulli(shape=(10,), p=1.0)
    assert sample.shape == (10, )
    assert jax.numpy.all(jax.numpy.equal(True, sample))


@pytest.mark.parametrize("mean1", [0, -5, 5])
@pytest.mark.parametrize("mean2", [5, 0, -5])
def test_draw_from_normal_conditional_on_sample(mean1, mean2):
    sut = SimulateMaxLikelihood(seed=42)
    sample = sut.draw_from_normal_conditional_on_sample(
        mask=numpy.array([True, False, True]), mean1=mean1, mean2=mean2
    )
    assert numpy.isclose(sample[0], mean2, atol=1)
    assert numpy.isclose(sample[2], mean2, atol=1)
    assert numpy.isclose(sample[1], mean1, atol=1)


def test_generate_log_r_samples():
    sut = SimulateMaxLikelihood(seed=43)
    xi, yi = sut.generate_log_r_sample(theta=numpy.array([1, 1, 1],
                                       dtype=numpy.float32),
                                       n=7)
    assert xi.shape == (7, 3)
    assert yi.shape == (7,)
