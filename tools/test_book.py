import os
import numpy as np
from collections import defaultdict

from .book import get_transition_counts, get_char_stream, counts_to_np, \
    smooth_transmat


def test_counts():
    transmat = get_transition_counts(1, "abcdefg")
    assert transmat['a']['b'] == 1
    assert transmat['a']['d'] == 0


def test_counts_2gram():
    transmat = get_transition_counts(2, "abcdefg")
    assert transmat['ab']['c'] == 1
    assert transmat['ab']['d'] == 0


def test_simple_char_stream(tmpdir):
    test_file_path = os.path.join(tmpdir, 'test.txt')
    with open(test_file_path, 'w') as outfile:
        outfile.write('abcdef')
    s = get_char_stream(test_file_path, 2)
    res = ''.join([char for char in s])
    assert res == 'abcdef'


def test_char_stream(tmpdir):
    test_file_path = os.path.join(tmpdir, 'test.txt')
    with open(test_file_path, 'w') as outfile:
        outfile.write('abC de2f')
    s = get_char_stream(test_file_path, 2)
    res = ''.join([char for char in s])
    assert res == 'abc def'


def test_counts_to_np():
    n_grams = ['a', 'b', 'c']
    counts = defaultdict(lambda: defaultdict(int), (
         ('a', defaultdict(int, (('b', 2), ('a', 1)))),
         ('b', defaultdict(int, (('a', 4), ('b', 3))))
    ))

    eta = 0.1
    transmat = counts_to_np(n_grams, counts)
    assert transmat.shape == (3, 27)
    assert transmat[0][0] == 1
    assert transmat[0][1] == 2
    assert transmat[1][0] == 4
    assert transmat[1][1] == 3
    assert transmat[2][1] == 0


def test_smooth_np():
    n_grams = ['a', 'b', 'c']
    counts = defaultdict(lambda: defaultdict(int), (
        ('a', defaultdict(int, (('b', 2), ('a', 1)))),
        ('b', defaultdict(int, (('a', 4), ('b', 3))))
    ))

    eta = 0.1
    smooth_np = smooth_transmat(counts_to_np(n_grams, counts), eta=eta)
    assert smooth_np[0][0] == np.divide((1 + 3 * eta/27), (3 + 3 * eta))
    assert smooth_np[0][1] == np.divide((2 + 3 * eta/27), (3 + 3 * eta))
    assert smooth_np[1][0] == np.divide((4 + 7 * eta/27), (7 + 7 * eta))
    assert smooth_np[1][1] == np.divide((3 + 7 * eta/27), (7 + 7 * eta))
    assert smooth_np[2][0] == 1/27

    assert np.sum(smooth_np[0]) == 1.
    assert np.sum(smooth_np[1]) == 1.
    # assert np.testing.assert_equal(np.sum(smooth_np[2]), 1.)

