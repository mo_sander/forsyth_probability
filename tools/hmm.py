from functools import lru_cache
from typing import List

from sklearn.metrics import mean_squared_error
import numpy as np
from hmmlearn import hmm


@lru_cache
def calculate_prob_14_1(n: int) -> float:
    """
    Calculates the probability of observing 5, 6 for the first time after n
    dice throws. The function used here differs from the book but
    rmse and sum of absolute errors are closer to the simulated values.
    """
    if n == 1:
        return 0
    if n == 2:
        return 1/36
    if n == 3:
        return 1/36
    return 5/6 * calculate_prob_14_1(n - 1) + 5/36 * calculate_prob_14_1(n - 2)


@lru_cache
def calculate_prob_14_2(n: int) -> float:
    """
    Calculates the probability of observing THT or HTH for the first time
    after n dice throws. The function used here differs from the book but
    rmse and sum of absolute errors are closer to the simulated values.
    """
    if n <= 2:
        return 0
    if n == 3:
        return 1/8 + 1/8
    return 1/2 * calculate_prob_14_2(n - 1) + 1/4 * calculate_prob_14_2(n - 2)


def initialise_hmm(emission_prob, start_prob, transmat):
    model = hmm.MultinomialHMM(n_components=len(start_prob))
    model.transmat_ = transmat
    model.startprob_ = start_prob
    model.emissionprob_ = emission_prob
    return model


def index_first_absorbing_state(Y: np.ndarray, absorbing_states: List[int]):
    candidates = [len(Y)]
    for s in absorbing_states:
        try:
            candidates.append(np.where(Y == s)[0][0])
        except IndexError:
            pass
    return min(candidates)


def hist_of_steps_to_end(model, its: int, absorbing_states: List[int], max_steps: int):
    steps = []
    for x in range(its):
        X, Y = model.sample(max_steps)
        steps.append(index_first_absorbing_state(Y, absorbing_states))
    hist = np.histogram(steps, bins=range(max_steps + 1))
    observed_probs = hist[0][:-1] / its
    print(f'encountered {hist[0][-1]} out of range events')
    return observed_probs


def simulate_dishonest_gambler(seq_len: int):
    model = dishonest_gambler_model()
    hidden_states = []
    emitted_states = []
    for x in range(10):
        X, Y = model.sample(seq_len)
        hidden_states.append(Y)
        emitted_states.append(X)

    predicted_states = []
    for e in emitted_states:
        Y_hat = model.predict(e)
        predicted_states.append(Y_hat)
    correct = np.count_nonzero(np.equal(predicted_states, hidden_states),
                               axis=1)
    print(f'The HMM gets it right for {np.mean(correct): .2f} of {seq_len} '
          f'steps.')
    assert True


def dishonest_gambler_model():
    transmat = np.array([[.5, .5],
                         [.5, .5]], dtype=np.float64)
    emission_prob = np.array([[1 / 6, 1 / 6, 1 / 6, 1 / 6, 1 / 6, 1 / 6],
                              [0.5, 0.1, 0.1, 0.1, 0.1, 0.1]], dtype=np.float64)
    start_prob = np.array([.5, .5], dtype=np.float64)
    model = initialise_hmm(emission_prob, start_prob, transmat)
    return model


def run_14_4a(max_steps: int):
    model = dishonest_gambler_model()

    X, Y = model.sample(max_steps)

    p1 = len(np.asarray(X == 0).nonzero()[0])
    print(f'Observed outcome "1" {p1} times in {max_steps} throws.')
    print(f'Expected to see "1" {max_steps/6:.2f} times with fair dice.')


def run_14_1():
    state_dict = {0: "O", 1: "5", 2: "6"}
    # noqa visualised here: https://setosa.io/markov/index.html#%7B%22tm%22%3A%5B%5B0.83%2C0.17%2C0%5D%2C%5B0.66%2C0.17%2C0.17%5D%2C%5B0%2C0%2C1%5D%5D%7D
    transmat = np.array([[5/6, 1/6, 0],
                         [4/6, 1/6, 1/6],
                         [0, 0, 1.0]], dtype=np.float32)
    emission_prob = np.identity(n=3, dtype=np.float32)
    start_prob = np.array([5/6, 1/6, 0], dtype=np.float32)
    model = initialise_hmm(emission_prob, start_prob, transmat)

    max_steps = 150
    observed_probs = hist_of_steps_to_end(model,
                                          its=1000,
                                          absorbing_states=[2],
                                          max_steps=max_steps)
    expected_probs = [calculate_prob_14_1(x + 1) for x in range(max_steps - 1)]
    print(f'MSE (%):'
          f'{mean_squared_error(observed_probs, expected_probs) * 100 :.5f} %')


def run_14_2():
    transmat = np.array([[.5, .0, .0, .5, .0, .0],
                         [.0, .5, .5, .0, .0, .0],
                         [.5, .0, .0, .0, .0, .5],
                         [.0, .5, .0, .0, .5, .0],
                         [.0, .0, .0, .0, 1., .0],
                         [.0, .0, .0, .0, .0, 1.]], dtype=np.float64)
    emission_prob = np.identity(n=6, dtype=np.float64)
    start_prob = np.array([.5, .5, .0, .0, .0, .0], dtype=np.float64)
    model = initialise_hmm(emission_prob, start_prob, transmat)

    max_steps = 10
    observed_probs = hist_of_steps_to_end(model,
                                          its=1000,
                                          absorbing_states=[4, 5],
                                          max_steps=max_steps)
    expected_probs = np.array([calculate_prob_14_2(x + 1) for x in range(
        max_steps - 1)], dtype=np.float32)
    print(f'MSE (%):'
          f'{mean_squared_error(observed_probs, expected_probs) * 100 :.5f} %')
    print(f'{np.sum(np.absolute((observed_probs - expected_probs))):.5f}')
    print(observed_probs - expected_probs)


def run_14_4_b():
    simulate_dishonest_gambler(100)


def run_14_4_c():
    simulate_dishonest_gambler(1000)


def main():
    # run_14_1()
    # run_14_2()
    # run_14_4a(2000)
    run_14_4_b()
    run_14_4_c()


if __name__ == "__main__":
    main()
