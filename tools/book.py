import re
from random import uniform, randint

import numpy as np
from collections import defaultdict
from itertools import product
from typing import Iterable, Generator, Dict, List, Literal

from hmmlearn import hmm
from tap import Tap

CHARSET = 'abcdefghijklmnopqrstuvwxyz '
CHARSET_RGX = re.compile(r'[a-z]| ')


class BookParser(Tap):
    filepath: str  # Path to book file
    eta: float = 0.0001  # Smoothing probability
    pc: float = 0.01  # error frequency
    n: Literal[1, 2, 3]  # n-gram size


def get_char_stream(filepath: str, buf: int = 10000) -> Generator[str, None,
                                                                None]:
    with open(filepath, 'r') as infile:
        while True:
            chars = infile.read(buf)
            if not chars:
                break
            chars = re.sub(r'\s+', ' ', chars.lower())
            for char in chars:
                if CHARSET_RGX.match(char):
                    yield char


def get_transition_counts(n: int, char_stream: Iterable[str]) -> Dict[str, Dict[str, int]]: # noqa
    buffer = ''
    transmat = defaultdict(lambda: defaultdict(int))
    for char in char_stream:
        if len(buffer) == n:
            transmat[buffer][char] += 1
            buffer = buffer[1:] + char
        else:
            buffer = buffer + char
    return transmat


def get_n_grams(n: int):
    charset = [x for x in CHARSET]
    return [''.join(x) for x in product(charset, repeat=n)]


def counts_to_np(n_grams: List[str], counts: Dict[str, Dict[str, int]]):
    transmat = np.zeros(shape=(len(n_grams), len(CHARSET)), dtype=np.float64)
    for i in range(len(n_grams)):
        for j in range(len(CHARSET)):
            transmat[i][j] = counts[n_grams[i]][CHARSET[j]]
    return transmat


def smooth_transmat(transmat: np.ndarray, eta: float):
    total_count = np.sum(transmat, axis=1).reshape(-1, 1)
    eta_abs_total = eta * total_count
    eta_abs_per_col = eta_abs_total / transmat.shape[1]
    transmat = np.divide(transmat + eta_abs_per_col, total_count + eta_abs_total)
    return np.nan_to_num(transmat,
                         nan=np.array([1.0], dtype=np.float64)
                             / np.array([transmat.shape[1]], dtype=np.float64)
                         )


def expand_transmat(transmat: np.ndarray, n_grams: List[str]):
    n_rows, n_cols = transmat.shape
    expanded_matrix = np.zeros(shape=(n_rows, n_rows))
    for i, igram in enumerate(n_grams):
        for j, jgram in enumerate(n_grams):
            if igram[1:] == jgram[:-1]:
                expanded_matrix[i][j] = transmat[i][CHARSET.index(jgram[-1])]
    return expanded_matrix


def get_emissions(n_grams: List[str], pc: float):
    emissions = np.zeros(shape=(len(n_grams), len(CHARSET)), dtype=np.float64)
    emissions = emissions + (pc / (len(CHARSET) - 1))
    for i in range(len(n_grams)):
        j = CHARSET.index(n_grams[i][-1])
        emissions[i][j] = 1 - pc
    return emissions


def initialise_hmm(emission_prob, transmat):
    model = hmm.MultinomialHMM(n_components=len(emission_prob))
    model.transmat_ = transmat
    model.startprob_ = np.sum(transmat, axis=0) / transmat.shape[1]
    model.emissionprob_ = emission_prob
    return model


def take_sample(length: int, offset: int, filepath: str) -> Generator[str,
                                                                      None,
                                                                      None]:
    char_stream = get_char_stream(filepath=filepath)
    x = 0
    while x < offset:
        next(char_stream)
        x += 1
    while x < offset + length:
        x += 1
        yield next(char_stream)


def perturb_sample(pc: float, sample: str):
    for char in sample:
        if uniform(0, 1) <= pc:
            char = CHARSET[randint(0, len(CHARSET) -1)]
        yield char


def mark_diffs(seqs: List[str]):
    diffs = ""
    for pos in zip(*seqs):
        if pos[0] == pos[2] and pos[0] != pos[1]:
            diffs += "*"
        elif pos[0] != pos[2]:
            diffs += "x"
        else:
            diffs += "-"
    return diffs


def get_aligned(seqs: List[str], chunk_size: int = 80):
    n=0
    l=1
    while l > 0:
        offset = n * chunk_size
        chunk = [seq[offset: offset + chunk_size] for seq in seqs]
        yield chunk
        n += 1
        l = len(chunk[0])


def main(file_path: str, eta: float, pc: float, n: int):
    book_stream = get_char_stream(file_path, buf=10000)
    n_grams = get_n_grams(n)
    transmat = expand_transmat(smooth_transmat(
        counts_to_np(n_grams, get_transition_counts(n, book_stream)), eta),
        n_grams)
    emissions = get_emissions(n_grams, pc=pc)
    model = initialise_hmm(emissions, transmat)
    X, Y = model.sample(500)
    print(''.join([CHARSET[x[0]] for x in X]))

    sample = ''.join(take_sample(200, 13400, file_path))
    perturbed_sample = ''.join(perturb_sample(pc, sample))
    X = model.predict([[CHARSET.index(x)] for x in perturbed_sample])
    diffs = mark_diffs([sample,
                        perturbed_sample,
                        ''.join([n_grams[x][-1] for x in X])])
    for seqs in get_aligned([sample,
                             perturbed_sample,
                             ''.join([n_grams[x][-1] for x in X]),
                            diffs]):
        for seq in seqs:
            print(seq)
        print()

    print(f"Corrected {diffs.count('*')} of "
          f"{diffs.count('*') + diffs.count('x')} "
          f"errors")


if __name__ == "__main__":
    args = BookParser().parse_args()
    main(args.filepath, eta=args.eta, pc=args.pc, n=args.n)
